#!/bin/bash -x

which apt /dev/null 2>&1
[[ $? -eq 0 ]] && pkg=apt
which yum >/dev/null 2>&1
[[ $? -eq 0 ]] && pkg=yum
which brew >/dev/null 2>&1
[[ $? -eq 0 ]] && pkg=brew

$pkg update
$pkg install zsh

if [[ ! -z "`which curl 2>/dev/null`" ]]; then
  sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
elif [[ ! -z "`which wget 2>/dev/null`" ]]; then
  sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"
fi
