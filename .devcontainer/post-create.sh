#!/usr/bin/env zsh

# package
task install

pre-commit install

# config
~/.dev/sync.sh
