var term = new Terminal();
term.open(document.getElementById('terminal'));

var shellprompt = '$ ';
term.prompt = function () {
  term.write('\r\n' + shellprompt);
};

term.clearPrompt = function() {
  term.write('\r')
  term.clear()
  term.writeln('\x1B[1;3;31mLfnt workstation\x1B[0m')
  term.prompt();
  term.setOption('cursorBlink', true);
}

var cmd = '';
term.clearPrompt()
